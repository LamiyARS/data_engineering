import logging
import sys
import warnings
from urllib.parse import urlparse

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.model_selection import train_test_split

import mlflow
import mlflow.sklearn
from mlflow.models import infer_signature

# Configure logging to write to a file
logging.basicConfig(level=logging.WARN, filename='model_training.log', filemode='w')
logger = logging.getLogger(__name__)

def eval_metrics(actual, pred):
    accuracy = accuracy_score(actual, pred)
    precision = precision_score(actual, pred, average='weighted')
    recall = recall_score(actual, pred, average='weighted')
    f1 = f1_score(actual, pred, average='weighted')
    return accuracy, precision, recall, f1

if __name__ == "__main__":
    warnings.filterwarnings("ignore")
    np.random.seed(40)

    # Set the tracking URI to local MLflow server with new port
    mlflow.set_tracking_uri("http://127.0.0.1:5000")

    # Read the csv file from the URL
    csv_url = "train.csv"
    try:
        data = pd.read_csv(csv_url, sep=",")
    except Exception as e:
        logger.exception(
            "Unable to download training & test CSV, check your internet connection. Error: %s", e
        )
        sys.exit(1)

    # Calculate mean value of the dataset
    mean_value = data.mean().mean()

    # Split the data into training and test sets. (0.75, 0.25) split.
    train, test = train_test_split(data)

    # The predicted column is "is_fraud" which is a binary label
    train_x = train.drop(['is_fraud'], axis=1)
    test_x = test.drop(['is_fraud'], axis=1)
    train_y = train['is_fraud']
    test_y = test['is_fraud']

    n_estimators = int(sys.argv[1]) if len(sys.argv) > 1 else 100
    max_depth = int(sys.argv[2]) if len(sys.argv) > 2 else None

    with mlflow.start_run():
        # Log the mean value of the dataset
        mlflow.log_metric("mean_value", mean_value)

        rf = RandomForestClassifier(n_estimators=n_estimators, max_depth=max_depth, random_state=42)
        rf.fit(train_x, train_y)

        predicted_qualities = rf.predict(test_x)

        (accuracy, precision, recall, f1) = eval_metrics(test_y, predicted_qualities)

        print(f"RandomForest model (n_estimators={n_estimators}, max_depth={max_depth}):")
        print(f"  Accuracy: {accuracy}")
        print(f"  Precision: {precision}")
        print(f"  Recall: {recall}")
        print(f"  F1 Score: {f1}")

        mlflow.log_param("n_estimators", n_estimators)
        mlflow.log_param("max_depth", max_depth)
        mlflow.log_metric("accuracy", accuracy)
        mlflow.log_metric("precision", precision)
        mlflow.log_metric("recall", recall)
        mlflow.log_metric("f1", f1)

        predictions = rf.predict(train_x)
        signature = infer_signature(train_x, predictions)

        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme

        if tracking_url_type_store != "file":
            mlflow.sklearn.log_model(
                rf, "model", registered_model_name="RandomForestClassifier", signature=signature
            )
        else:
            mlflow.sklearn.log_model(rf, "model", signature=signature)
