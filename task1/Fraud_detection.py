import pandas as pd
import numpy as np
import pickle
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder, StandardScaler, FunctionTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
import joblib
import os

path='/Users/nabadovalamiya/Downloads/credit-card-fraud-detection-scikitlearn-credit-card-fraud-detection-v1/fraudTrain.csv'
df1=pd.read_csv(path)
selected_features=['cc_num','merchant', 'category','amt','lat', 'long','job','dob','unix_time','merch_lat', 'merch_long','is_fraud']
df=df1[selected_features]

def add_age_group(df):
    # Calculate age from 'dob'
    df['dob'] = pd.to_datetime(df['dob'])
    today = pd.to_datetime('today')
    df['age'] = (today - df['dob']).astype('<m8[Y]')
    
    # Define age bins and labels
    bins = [0, 25, 35, 45, 100]
    labels = ['<25', '25-35', '35-45', '45+']
    
    # Add 'age_group' feature
    df['age_group'] = pd.cut(df['age'], bins=bins, labels=labels, right=False)
    
    # Drop 'dob' column
    df.drop(columns=['dob', 'age'], inplace=True)
    
    return df

# Function to be used in the pipeline
def add_age_group_to_pipeline(df):
    return add_age_group(df)

# Create a FunctionTransformer
age_group_transformer = FunctionTransformer(add_age_group_to_pipeline)

# Create a pipeline
pipe1 = Pipeline(steps=[
    ('add_age_group', age_group_transformer)
])

transformed_df = pipe1.fit_transform(df)
transformed_df.columns

# Step 1: Filter rows where is_fraud == 1
fraud_df = transformed_df[transformed_df['is_fraud'] == 1]
# Step 2: Sample an equal number of rows where is_fraud == 0
non_fraud_df = transformed_df[transformed_df['is_fraud'] == 0].sample(n=8000, random_state=42)
# Step 3: Combine the two DataFrames
balanced_df = pd.concat([fraud_df, non_fraud_df])
# Optionally shuffle the DataFrame to mix the rows
balanced_df = balanced_df.sample(frac=1, random_state=42).reset_index(drop=True)


X = balanced_df.drop(columns='is_fraud')
y = balanced_df['is_fraud']

preprocessor = ColumnTransformer(
    transformers=[
        ('num', StandardScaler(), ['amt',  'lat', 'long', 'unix_time', 'merch_lat', 'merch_long'])
    ])

pipeline = Pipeline(steps=[
    ('preprocessor', preprocessor),
    ('classifier', RandomForestClassifier())
])
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Train the model
pipeline.fit(X_train, y_train)

# Predict on the test set
y_pred = pipeline.predict(X_test)

# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)
precision = precision_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)

# Display the results
print(f"Model Accuracy: {accuracy}")
print(f"Model F1 Score: {f1}")
print(f"Model Precision: {precision}")
print(f"Model Recall: {recall}")

save_directory = '/Users/nabadovalamiya/Desktop/fraud'

# Define the full path for the saved model
model_path = os.path.join(save_directory, 'fraud_detection_pipeline.pkl')

# Save the model to the specified directory
joblib.dump(pipeline, model_path)