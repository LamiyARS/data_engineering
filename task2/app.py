from flask import Flask, request, jsonify
import joblib
import pandas as pd
import logging

app = Flask(__name__)

logging.basicConfig(level=logging.INFO)


model = joblib.load('fraud_detection_pipeline.pkl')

@app.route('/predict', methods=['POST'])
def predict():
    try:
        data = request.get_json() 
        if not data:
            return jsonify({"error": "No input data provided"}), 400
        
        logging.info(f"Received data: {data}")

        if isinstance(data, dict):
            data = [data]
        df = pd.DataFrame(data)
        logging.info(f"DataFrame created: {df}")
        predictions = model.predict(df)
        return jsonify({'predictions': predictions.tolist()})
    except Exception as e:
        logging.error(f"Error during prediction: {e}")
        return jsonify({"error": "Error during prediction"}), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)




